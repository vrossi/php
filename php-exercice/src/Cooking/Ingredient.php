<?php

namespace Add\Cooking;

class Ingredient {
public $name;
public $calories;
public $vegetal;
public $raw;

public function __construct(string $name, int $calories, bool $vegetal, bool $raw) {
  $this->name = $name;
  $this->calories = $calories;
  $this->vegetal = $vegetal;
  $this->raw = $raw;
     
}

public function cook(){
if ($this->raw === false) {
$this->raw = true;
}
else {
$this->calories++;
}
}
}

// La méthode cook permettra de faire passer la propriété raw de false à true si elle n'est pas déjà true, et fera augmenter un peu les calories)